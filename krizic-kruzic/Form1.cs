﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krizic_kruzic
{
    public partial class Form1 : Form
    {
        List<Polje> lista = new List<Polje>();
        Graphics p1,p2,p3,p4,p5,p6,p7,p8,p9;

        private void pb3_Click(object sender, EventArgs e)
        {
            if (lista[2].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                    Krizic a = new Krizic();
                    a.nacrtaj(p3);
                    lista[2].Znak = 'x';
                    if (lista[0].Znak == 'x' && lista[1].Znak == 'x')
                    {
                        pb3.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[5].Znak == 'x' && lista[8].Znak == 'x')
                    {
                        pb3.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[4].Znak == 'x' && lista[6].Znak == 'x')
                    {
                        pb3.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;

                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p3);
                    lista[2].Znak = 'o';
                    if (lista[1].Znak == 'o' && lista[0].Znak == 'o')
                    {
                        pb3.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[5].Znak == 'o' && lista[8].Znak == 'o')
                    {
                        pb3.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[4].Znak == 'o' && lista[6].Znak == 'o')
                    {
                        pb3.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;
                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void pb4_Click(object sender, EventArgs e)
        {
            if (lista[3].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                    Krizic a = new Krizic();
                    a.nacrtaj(p4);
                    lista[3].Znak = 'x';
                    if (lista[0].Znak == 'x' && lista[6].Znak == 'x')
                    {
                        pb4.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[4].Znak == 'x' && lista[5].Znak == 'x')
                    {
                        pb4.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;


                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p4);
                    lista[3].Znak = 'o';
                    if (lista[0].Znak == 'o' && lista[6].Znak == 'o')
                    {
                        pb4.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[4].Znak == 'o' && lista[5].Znak == 'o')
                    {
                        pb4.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;

                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void pb5_Click(object sender, EventArgs e)
        {
            if (lista[4].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                   
                    Krizic a = new Krizic();
                    a.nacrtaj(p5);
                    lista[4].Znak = 'x';
                    if (lista[1].Znak == 'x' && lista[7].Znak == 'x')
                    {
                        pb5.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[3].Znak == 'x' && lista[5].Znak == 'x')
                    {
                        pb5.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[0].Znak == 'x' && lista[8].Znak == 'x')
                    {
                        pb5.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;

                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p5);
                    lista[4].Znak = 'o';
                    if (lista[1].Znak == 'o' && lista[7].Znak == 'o')
                    {
                        pb5.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[3].Znak == 'o' && lista[5].Znak == 'o')
                    {
                        pb5.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[0].Znak == 'o' && lista[8].Znak == 'o')
                    {
                        pb5.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;
                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void pb6_Click(object sender, EventArgs e)
        {
            if (lista[5].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                  
                    Krizic a = new Krizic();
                    a.nacrtaj(p6);
                    lista[5].Znak = 'x';
                    if (lista[2].Znak == 'x' && lista[8].Znak == 'x')
                    {
                        pb6.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[3].Znak == 'x' && lista[4].Znak == 'x')
                    {
                        pb6.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;


                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p6);
                    lista[5].Znak = 'o';
                    if (lista[2].Znak == 'o' && lista[8].Znak == 'o')
                    {
                        pb6.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[3].Znak == 'o' && lista[4].Znak == 'o')
                    {
                        pb6.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;

                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void pb7_Click(object sender, EventArgs e)
        {
            if (lista[6].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                    Krizic a = new Krizic();
                    a.nacrtaj(p7);
                    lista[6].Znak = 'x';
                    if (lista[4].Znak == 'x' && lista[2].Znak == 'x')
                    {
                        pb7.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[3].Znak == 'x' && lista[0].Znak == 'x')
                    {
                        pb7.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[7].Znak == 'x' && lista[8].Znak == 'x')
                    {
                        pb7.BackColor = Color.Green;
                        pb8.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;

                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p7);
                    lista[6].Znak = 'o';
                    if (lista[4].Znak == 'o' && lista[2].Znak == 'o')
                    {
                        pb7.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[3].Znak == 'o' && lista[0].Znak == 'o')
                    {
                        pb7.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[7].Znak == 'o' && lista[8].Znak == 'o')
                    {
                        pb7.BackColor = Color.Green;
                        pb8.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;
                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void pb8_Click(object sender, EventArgs e)
        {
            if (lista[7].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                    Krizic a = new Krizic();
                    a.nacrtaj(p8);
                    lista[7].Znak = 'x';
                    if (lista[1].Znak == 'x' && lista[4].Znak == 'x')
                    {
                        pb8.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[8].Znak == 'x' && lista[6].Znak == 'x')
                    {
                        pb8.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;


                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p8);
                    lista[7].Znak = 'o';
                    if (lista[1].Znak == 'o' && lista[4].Znak == 'o')
                    {
                        pb8.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[8].Znak == 'o' && lista[6].Znak == 'o')
                    {
                        pb8.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;

                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void pb9_Click(object sender, EventArgs e)
        {
            if (lista[8].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                    Krizic a = new Krizic();
                    a.nacrtaj(p9);
                    lista[8].Znak = 'x';
                    if (lista[0].Znak == 'x' && lista[4].Znak == 'x')
                    {
                        pb9.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[2].Znak == 'x' && lista[5].Znak == 'x')
                    {
                        pb9.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[6].Znak == 'x' && lista[7].Znak == 'x')
                    {
                        pb9.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        pb8.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;

                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p9);
                    lista[8].Znak = 'o';
                    if (lista[0].Znak == 'o' && lista[4].Znak == 'o')
                    {
                        pb9.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[2].Znak == 'o' && lista[5].Znak == 'o')
                    {
                        pb9.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        pb6.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[6].Znak == 'o' && lista[7].Znak == 'o')
                    {
                        pb9.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        pb8.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;
                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void btn_igraj_ponovo_Click(object sender, EventArgs e)
        {
            broj_poteza = 0;
            for(int i = 0; i < 9; i++)
            {
                lista[i].Znak ='n';
            }
            pb1.Image=null;
            pb2.Image = null;
            pb3.Image = null;
            pb4.Image = null;
            pb5.Image = null;
            pb6.Image = null;
            pb7.Image = null;
            pb8.Image = null;
            pb9.Image = null;
            pb1.BackColor = Color.White;
            pb2.BackColor = Color.White;
            pb3.BackColor = Color.White;
            pb4.BackColor = Color.White;
            pb5.BackColor = Color.White;
            pb6.BackColor = Color.White;
            pb7.BackColor = Color.White;
            pb8.BackColor = Color.White;
            pb9.BackColor = Color.White;
            lb_potez_igrac.Text = lb_igrac1_ime.Text;

        }

        private void btn_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pb2_Click(object sender, EventArgs e)
        {
            if (lista[1].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                    Krizic a = new Krizic();
                    a.nacrtaj(p2);
                    lista[1].Znak = 'x';
                    if (lista[0].Znak == 'x' && lista[2].Znak == 'x')
                    {
                        pb2.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[4].Znak == 'x' && lista[7].Znak == 'x')
                    {
                        pb2.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb8.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;


                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p2);
                    lista[1].Znak = 'o';
                    if (lista[0].Znak == 'o' && lista[2].Znak == 'o')
                    {
                        pb2.BackColor = Color.Green;
                        pb1.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[4].Znak == 'o' && lista[7].Znak == 'o')
                    {
                        pb2.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb8.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;

                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");

        }

        private void pb1_Click(object sender, EventArgs e)
        {
            if (lista[0].Znak == 'n')
            {
                if (broj_poteza % 2 == 0)
                {
                    Krizic a = new Krizic();
                    a.nacrtaj(p1);
                    lista[0].Znak = 'x';
                    if (lista[1].Znak == 'x' && lista[2].Znak == 'x')
                    {
                        pb1.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[3].Znak == 'x' && lista[6].Znak == 'x')
                    {
                        pb1.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    if (lista[4].Znak == 'x' && lista[8].Znak == 'x')
                    {
                        pb1.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac1_rez++;
                        lb_igrac1_b.Text = igrac1_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac2_ime.Text;

                }
                else
                {
                    Circle a = new Circle();
                    a.nacrtaj(p1);
                    lista[0].Znak = 'o';
                    if (lista[1].Znak == 'o' && lista[2].Znak == 'o')
                    {
                        pb1.BackColor = Color.Green;
                        pb2.BackColor = Color.Green;
                        pb3.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[3].Znak == 'o' && lista[6].Znak == 'o')
                    {
                        pb1.BackColor = Color.Green;
                        pb4.BackColor = Color.Green;
                        pb7.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    if (lista[4].Znak == 'o' && lista[8].Znak == 'o')
                    {
                        pb1.BackColor = Color.Green;
                        pb5.BackColor = Color.Green;
                        pb9.BackColor = Color.Green;
                        MessageBox.Show("Pobjeda!");
                        igrac2_rez++;
                        lb_igrac2_b.Text = igrac2_rez.ToString();

                    }
                    lb_potez_igrac.Text = lb_igrac1_ime.Text;
                }
                broj_poteza++;
                if (broj_poteza == 9)
                    MessageBox.Show("Nerijeseno!");
            }
            else
                MessageBox.Show("Ovo polje je vec zauzeto!");
        }

        private void btn_dodaj_Click(object sender, EventArgs e)
        {
            if (lb_unosimena.Text == "Ime prvog igraca:")
            {
                lb_igrac1_ime.Text = tb_imena.Text;
                lb_unosimena.Text = "Ime drugog igraca:";

            }
            else
            {
                lb_igrac2_ime.Text = tb_imena.Text;
                
            }
            lb_potez_igrac.Text = lb_igrac1_ime.Text;
        }

        int broj_poteza =0;
        int igrac1_rez=0;
        int igrac2_rez = 0;
        


        public Form1()
        {
            InitializeComponent();
            p1 = pb1.CreateGraphics();
            p2 = pb2.CreateGraphics();
            p3 = pb3.CreateGraphics();
            p4 = pb4.CreateGraphics();
            p5 = pb5.CreateGraphics();
            p6 = pb6.CreateGraphics();
            p7 = pb7.CreateGraphics();
            p8 = pb8.CreateGraphics();
            p9 = pb9.CreateGraphics();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 9; i++)
            {
                Polje p = new Polje();
                lista.Add(p);
            }
           
        }
    }

    class Polje
    {
       
        private char znak;
        public Polje()
        {
          
            znak = 'n';
        }
       
        public char Znak
        {
            get
            {
                return znak;
            }
            set
            {
                znak = value;
            }
        }
    }

    class Circle
    {
        private int r;
        public Circle()
        {
            r = 25;
        }
        public void nacrtaj(Graphics g)
        {
            Pen p = new Pen(Color.Blue, 2F);
            g.DrawEllipse(p, 10, 15, r, r);
        }
    }
    class Krizic
    {
     
        public void nacrtaj(Graphics g)
        {
            Pen p = new Pen(Color.Red, 2F);
            g.DrawLine(p,12,12,40,40);
            g.DrawLine(p, 12, 40, 40,12);

        }
    }
}
